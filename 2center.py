import time
import sys
import numpy as np

# compute the cost of using centers c1, c2
def centers_cost_sq(pts,c1,c2):
    cost_sq = 0
    n = len(pts)
    for i in range(n):
        dist_sq_1 = np.inner(pts[i]-pts[c1],pts[i]-pts[c1])
        dist_sq_2 = np.inner(pts[i]-pts[c2],pts[i]-pts[c2])
        min_dist_sq = min(dist_sq_1,dist_sq_2)
        cost_sq = max(cost_sq,min_dist_sq)
    return cost_sq

# find the optimal cost by checking each pair of centers
def solve_2center(pts,opt_ctrs):
    min_cost_sq = float("inf")
    n = len(pts)
    tuples_checked = 0
    for i in range(0,n-1):
        for j in range(i+1,n):
            cost_sq = centers_cost_sq(pts,i,j)
            tuples_checked += 1
            if (cost_sq < min_cost_sq):
                min_cost_sq = cost_sq
                opt_ctrs[0] = i
                opt_ctrs[1] = j
    return (np.sqrt(min_cost_sq),tuples_checked)

pts = np.loadtxt(sys.argv[1])
opt_ctrs = np.array([0,0])
start = time.process_time()
(min_cost,tuples_checked) = solve_2center(pts,opt_ctrs)
elapsed = time.process_time()
print ('number of points =',len(pts))
print ('2-tuples checked =',tuples_checked)
print ('elapsed time =',np.round(elapsed,5),'seconds')
print ('2-tuples checked per second =',int(tuples_checked/elapsed))
print ('minimum cost =',np.round(min_cost,5))
print ('optimal centers =',opt_ctrs)
